export default {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: 'ＬＥＮＳ｜ポストに届く1DAYレンズ定期便' || '',
    htmlAttrs: {
      lang: 'ja',
    },
    script: [
      { id: 'ebis_common', src: 'ebis.js'},
      { type: 'text/javascript', id:'ebis' }
    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    '@/assets/scss/global/app.scss'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "~/plugins/axios",
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxtjs/moment'
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    // '@nuxtjs/stylelint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    '@nuxtjs/proxy',
    'cookie-universal-nuxt',
    ['@nuxtjs/google-tag-manager', { id: 'GTM-WKBSZB5', pageTracking: true }],
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true,
  },
  proxy: {
    "/api/v1": {
      target: process.env.ApiUrl,
      pathRewrite: {
        '^/api/v1/': '/api/v1/'
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) { }
  },
  router: {
    middleware: ["auth-cookie", "authentication"],
  },
  styleResources: {
    scss: [
      'assets/scss/includes/_includes.scss',
    ]
  },
  moment: {
    defaultLocale: 'ja',
    locales: ['ja']
  },
  env:{
    paymentUrl:process.env.baseUrl,
    token: process.env.token
  },
  // 追記
  generate: {
    dir: '../public'
  }
}