import Cookies from "universal-cookie"

export const state = () => ({
  isLoggedIn: false,
  userToken: '',
  isLoginFaild: false,
  isLoginSend: false,
  isLogout: false,
  isInprogress: false,
  isSucess: false,
  isAuth: false,
});

export const getters = {
  isLoggedIn: (state) => state.isLoggedIn,
  isLoginSend: (state) => state.isLoginSend,
  isLoginFaild: (state) => state.isLoginFaild,
  isLogout: (state) => state.isLogout,
  isInprogress: (state) => state.isInprogress,
  isSucess: (state) => state.isSucess,
  userToken: (state) => state.userToken,
  isAuth: (state) => state.isAuth,
};

export const mutations = {
  setLogin(state, token) {
    state.userToken = token;
    state.isLogout = false;
    state.isLoggedIn = true;
  },
  clearLogin(state) {
    state.token = '';
    state.isLoggedIn = false;
    state.isLogout = true;
  },
  clearLogout(state) {
    state.isLogout = false;
  },
  setLoginSend(state, status) {
    state.isLoginSend = status;
  },
  setIsInprogress(state, status) {
    state.isInprogress = status;
  },
  setIsSucess(state, status) {
    state.isSucess = status;
  },
  setLoginFaild(state, status) {
    state.isLoginFaild = status;
  },
  setIsAuth(state, status) {
    state.isAuth = status;
  },
};

export const actions = {
  login({ commit }, { email, password }) {
    const data = {
      email,
      password,
      platform: 'web',
      platform_type: 0,
    };
    const userToken = this.$axios.$post('/api/v1/login', data);
    commit('setLoginSend', true);
    commit('setLoginFaild', false)
    commit('clearLogout')
    userToken.then(res => {
      this.$cookies.remove('user_token')
      this.$cookies.set('user_token', JSON.stringify(res.token), {
        maxAge: 60 * 60 * 24 * 30, // expire after 1 month or 30 days
        path: '/'
      })
      this.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + res.token;
      this.$axios.defaults.headers.common['platform'] = 'web';
      commit('setLoginSend', false);
      commit('setLogin', res.token);
    }).catch(err => {
      commit('setLoginFaild', true);
      commit('setLoginSend', false);
    });
  },
  logout({ commit }, authFlag) {
    commit('clearLogin');
    commit('setIsAuth', authFlag)
    this.$cookies.remove('user_token')
    if (authFlag){
      this.$router.push({
        path: '/login',
        meta: {
          isLogout: true,
        }
      });
    }else{
      window.location.href = '/index'
    }
    
  },
  softLogout({ commit }) {
    commit('clearLogin');
    this.$cookies.remove('user_token');
    commit('clearLogout');
  },
  clearIsLogout({ commit }) {
    commit('clearLogout');
  },
  clearLoginMessage({ commit }) {
    commit('setLoginFaild', false);
  },
  passwordResetToken({ commit }, email) {
    const resetToken = this.$axios.$post('/api/v1/reset_token', {
      email,
      platform: 'web',
    });
    commit('setIsInprogress', true);
    resetToken.then(res => {
      commit('setIsInprogress', false);
      if (res.success) {
        this.$router.push('/login');
      }
    }).catch(err => {
      commit('setIsInprogress', false);
    })
  },
  passwordReset({ commit }, { reset_token, new_password, new_password_confirmation }) {
    const resetPassword = this.$axios.$post('/api/v1/reset_password', {
      reset_token,
      new_password,
      new_password_confirmation
    });
    commit('setIsInprogress', true);
    resetPassword.then(res => {
      this.$router.push('/login');
      commit('setIsInprogress', false);

    }).catch(err => {
      commit('setIsInprogress', false);
    })
  },
  updatePassword({ commit }, { current_password, new_password }) {
    commit('setIsSucess', false);
    commit('setIsInprogress', true);
    const updatePass = this.$axios.$put('/api/v1/update_password', {
      current_password,
      new_password
    });
    
    updatePass.then(res => {
      commit('setIsInprogress', false);
      commit('setIsSucess', true);
      
    }).catch(err => {
      commit('setIsInprogress', false);
      commit('setIsSucess', false);
    })
  }
}