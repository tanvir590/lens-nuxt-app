
export const state = () => ( {
  regestration: {
    email: '',
    password: '',
    name: '',
    zipcode: '',
    prefecture: '',
    address_1: '',
    address_2: '',
    phone_number: '',
    left_lens_id: '',
    right_lens_id: '',
    invite_code: '',
    token: '',
  },
  payment:{},
  status: {
    lens: false,
    address: false,
    user: false,
    temp: false,
    payment: false,
    reg: false,
  },
  errorMessages: [],
  isError: false,
  checkStatus: false,
  checkEmail: false,
  checkRegestration: false,
  checkCard: false,
  checkUser: false,
  isInProgress: false,
  orderStatus: 0,
  creditComp: '',
  userId: '',
} );

export const getters = {
  isInProgress: state => state.isInProgress,
  orderStatus: state => state.orderStatus,
  getRegestration: state => state.regestration,
  getPayment: state => state.payment,
  getStatus: state => state.checkStatus,
  getEmail: state => state.checkEmail,
  getTemp: state => state.checkRegestration,
  getCard: state => state.checkCard,
  getUserInfo: state => state.checkUser,
  status: state => state.status,
  errorMessages: state => state.errorMessages,
  isError: state => state.isError,
  creditComp: state => state.creditComp,
  userId: state => state.userId
};

export const mutations = {
  setInProgress( state, status ) {
    state.isInProgress = status;
  },
  setOrderStatus( state, status ) {
    state.orderStatus = status;
  },
  setRegestration( state, regestration){
    state.regestration=regestration;
  },
  setRegestrationItem( state, { valueId, value }){
    state.regestration= {
      ...state.regestration,
      [valueId]: value,
    };
  },
  setRegestrationItems( state, newValues){
    state.regestration= {
      ...state.regestration,
      ...newValues
    };
  },
  setStepStatus( state, status ) {
    state.status= {
      ...state.status,
      ...status
    };
  },
  setPayment(state, payment){
    state.payment=payment;
    state.regestration.token=payment.token;
  },
  setStatus(state, status){
    state.checkStatus=status
  },
  setEmail(state,status){
    state.checkEmail=status
  },
  setTemp(state,status){
    state.checkRegestration=status
  },
  setCard(state,status){
    state.checkCard=status
  },
  setUserInfo(state,status){
    state.checkUser=status
  },
  setIsError(state,status){
    state.isError=status
  },
  setErrorMessages(state,errorMessages) {
    state.errorMessages=errorMessages
  },
  resetState( state ) {
    state = {
      regestration: {
        email: '',
        password: '',
        name: '',
        zipcode: '',
        prefecture: '',
        address_1: '',
        address_2: '',
        phone_number: '',
        left_lens_id: '',
        right_lens_id: '',
        invite_code: '',
        token: '',
      },
      payment:{},
      status: {
        lens: false,
        address: false,
        user: false,
        temp: false,
        payment: false,
        reg: false,
      },
      errorMessages: [],
      isError: false,
      checkStatus: false,
      checkEmail: false,
      checkRegestration: false,
      checkCard: false,
      checkUser: false,
      isInProgress: false,
      orderStatus: 0,
    };
  },
  setCreditComp(state, comp) {
    state.creditComp = comp
  },
  setUserId(state, userId) {
    state.userId = userId
  }
};

export const actions = {
  fetchOrderStatus( { commit } ) {
    const orderStatus = this.$axios.$get( '/api/v1/order_status' );
    commit( 'setInProgress', true );

    orderStatus.then( res => {
      commit( 'setOrderStatus', res.status );
      commit( 'setInProgress', false );
    } ).catch( () => {
      commit('setInProgress', false)
    } );
  },
  updateOrderStatus( { commit }, status ) {
    const orderStatus = this.$axios.$put( '/api/v1/order_status', { status } );
    commit( 'setInProgress', true );

    orderStatus.then( res => {
      commit( 'setOrderStatus', res.status );
      commit( 'setInProgress', false );
    } );
  },
  setLens( { commit }, { left_lens_id, right_lens_id } ) {
    const data = {
      left_lens_id,
      right_lens_id
    };

    commit('setStepStatus', {
      lens: true
    })
    commit('setRegestrationItems',data);

    this.$router.push( '/order/address' );
  },
  setAddress( { commit }, { name, zipcode, prefecture, phone_number, address_1, address_2 } ) {
    const data = { name, zipcode, prefecture, phone_number, address_1, address_2 };
    const formData = new FormData();
    Object.keys( data ).forEach( dataKey => {
      formData.append( dataKey, data[dataKey] )
    } )
    const check_user=this.$axios.$post('/api/v1/validate_user_info',formData);
    commit('setInProgress', true)
    commit('setIsError', false)
    check_user.then(res => {
      if(res.valid == true){
        commit( 'setRegestrationItems', data );
        commit('setStepStatus', {
          address: true
        })
        this.$router.push( '/order/credentials' );
      } else {
        commit('setIsError', true)
        commit('setErrorMessages', res.error_field.map( i => i.message ))
      }
      commit('setInProgress', false)
    }).catch( () => {
      commit('setInProgress', false)
      commit('setIsError', true)
      commit('setErrorMessages', [ 'Something wrong, try again?'])
    } )
    
  },

  setUser( { commit, state }, { email, password, invite_code } ) {
    const data = { email, password, invite_code };

    const valid_login=this.$axios.$post('/api/v1/validate_mail_password',data);
    commit('setInProgress', true)
    commit('setIsError', false)
    valid_login.then(res => {
      if(res.valid == true){
        commit('setRegestrationItems',data);

        const temp_register=this.$axios.$post('/api/v1/tmp_register',state.regestration)
        temp_register.then(res => {
          if(res.success == true){
            commit('setStepStatus', {
              user: true
            })
            this.$router.push( '/order/payment_info' );
          }
          commit('setInProgress', false)
        }).catch( ( err ) => {
          commit('setInProgress', false)
          commit('setIsError', true)
          commit('setErrorMessages', [ err.response.data.message ] )
        } )
      } else {
        commit('setIsError', true)
        commit('setErrorMessages', res.error_field.map( i => i.message ))
      }
      commit('setInProgress', false)
    }).catch( () => {
      commit('setInProgress', false)
      commit('setIsError', true)
      commit('setErrorMessages', [ 'Something wrong, try again?'])
    } )
  },
  setPayment( { commit }, payment ) {
    commit('setPayment', payment)
    commit('setStepStatus', {
      temp: true
    });
    commit('setInProgress', false)
  },
  checkRegestration({commit},payload){
    const user_info={};
    user_info.name=payload.name
    user_info.zipcode=payload.zipcode
    user_info.prefecture=payload.prefecture
    user_info.phone_number=payload.phone_number
    user_info.address_1=payload.address_1
    user_info.address_2=payload.address_2
    user_info.check_email=payload.email
    const check_user=this.$axios.$post('/api/v1/validate_user_info',user_info);
    check_user.then(res => {
      if(res.valid == true){
        commit('setStatus',true)
      }
    })
  },
  checkMail({commit},payload){
    const check_valid={};
    check_valid.email= payload.email
    check_valid.password= payload.password
    const valid_login=this.$axios.$post('/api/v1/validate_mail_password',check_valid);
    valid_login.then(res => {
      if(res.valid == true){
        commit('setRegestration',payload);
        commit('setEmail',true)
      }
    })  
  },
  tempRegestration({commit},payload){
    const temp={};
    temp.email=payload.email
    temp.password=payload.password
    temp.name=payload.name
    temp.zipcode=payload.zipcode
    temp.prefecture=payload.prefecture
    temp.address_1=payload.address_1
    temp.address_2=payload.address_2
    temp.phone_number=payload.phone_number
    temp.invite_code=payload.invite_code
    temp.left_lens_id=payload.left_lens_id
    temp.right_lens_id=payload.right_lens_id
    const temp_register=this.$axios.$post('/api/v1/tmp_register',temp)
    commit('setInProgress', true)
    temp_register.then(res => {
      if(res.success == true){
        commit('setTemp',true)
      }
      commit('setInProgress', false)
    }).catch( () => {
      commit('setInProgress', false)
    } )
  },
  cardValid({commit,state}){
    const card={};
    card.email=state.regestration.email
    card.password=state.regestration.password
    card.token=state.payment.token
    const valid_card = this.$axios.$post('/api/v1/credit_validate',card);
    commit('setInProgress', true)
    commit('setIsError', false)
    valid_card.then(res =>{
      if(res.success == true){
        commit('setCard',true)
        commit('setStepStatus', {
          payment: true
        });
        this.$router.push( '/order/confirm' );
      } else {
        commit('setIsError', true)
        commit('setErrorMessages', [ res.message ] )
      }
      commit('setInProgress', false)
    }).catch( ( err ) => {
      commit('setInProgress', false)
      commit('setIsError', true)
      commit('setErrorMessages', [ err.response.data.message ] )
    } )
  },
  sendRegestration({commit,state},payload){
    const register={};
    register.email=payload.email
    register.password=payload.password
    register.name=payload.name
    register.zipcode=payload.zipcode
    register.prefecture=payload.prefecture
    register.address_1=payload.address_1
    register.address_2=payload.address_2
    register.phone_number=payload.phone_number
    register.left_lens_id=payload.left_lens_id
    register.right_lens_id=payload.right_lens_id
    register.invite_code=payload.invite_code
    register.token=state.payment.token
    const regestration=this.$axios.$post('/api/v1/user',register);
    commit('setInProgress', true)
    commit('setIsError', false)
    regestration.then(res =>{
      if(res.analytics_state == 1)
      {
        commit('setUserId', res.id)
        var ebisHtml = document.getElementById('ebis');
        ebisHtml.innerHTML = `window.ebis=window.ebis||[];ebis.push({argument: '8t1jWo71',member_name: '${res.id}',amount: '',other1: '',other2: '',other3: '',other4: '',other5: '',});`
        ebisHtml.insertAdjacentHTML('afterend', `<input type="hidden" name="user_id" value="${res.id}"/>`);
        commit('setUserInfo',true)
        commit('setStepStatus', {
          reg: true
        });
      }
      commit('setInProgress', false)
    }).catch( ( err ) => {
      commit('setInProgress', false)
      commit('setIsError', true)
      commit('setErrorMessages', [ err.response.data.message ])
    } )
  },
  getCreditComp({ commit }, cardNo) {
    const patternVisa = /^4[0-9]{12}(?:[0-9]{3})?$/
    const patternMaster = /^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/
    const patternJCB = /^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/
    const patternAmerica = /^3[47][0-9]{13}$/
    var comp = ''

    if (patternVisa.test(cardNo)) comp = 'Visa'
    else if (patternMaster.test(cardNo)) comp = 'MasterCard'
    else if (patternJCB.test(cardNo)) comp = 'JCB'
    else if (patternAmerica.test(cardNo)) comp = 'America Express'
    else comp = ''

    commit('setCreditComp', comp)
  }
};
