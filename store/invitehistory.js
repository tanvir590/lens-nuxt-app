
export const state = () => ({
  isInProgress: false,
  inviteHistory: [{}],
  responseStatusCode: ''
});

export const getters = {
  isInProgress: state => state.isInProgress,
  inviteHistory: state => state.inviteHistory,
  responseStatusCode: state => state.responseStatusCode,
};

export const mutations = {
  setInviteHistory(state, history) {
    state.inviteHistory = history;
  },
  setInProgress(state, status) {
    state.isInProgress = status;
  },
  setResponseStatusCode(state, status){
    state.responseStatusCode = status
  }
};

export const actions = {
  fetchInviteHistory({ commit }) {
    commit('setResponseStatusCode', 0)
    var resp = this.$axios.$get('/api/v1/invite_info')
    commit('setInProgress', true)
    resp.then((res) => {
      commit('setInviteHistory',res['invite_infos'])
      commit('setInProgress', false)
      commit('setResponseStatusCode', 200)
    }).catch((err) => {
      commit('setInProgress', false)
      commit('setResponseStatusCode', err.response.status)
    })
  }
};
