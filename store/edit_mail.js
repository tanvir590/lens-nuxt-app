
export const state = () => ({
  isInProgress: false,
  isError: false,
  isSucess: false
});

export const getters = {
  isInProgress: state => state.isInProgress,
  isError: state => state.isError,
  isSucess: state => state.isSucess

};

export const mutations = {

  setInProgress(state, isInProgress) {
    state.isInProgress = isInProgress;
  },
  setError(state, isError) {
    state.isError = isError;
  },
  isSucess(state, status) {
    state.isSucess = status
  }
};

export const actions = {
  updateUserMail({ commit }, payload) {
    commit('setInProgress', true);
    commit('setError', false);
    commit('isSucess', false)
    const {
      email,
      password
    } = payload
    const userData = this.$axios.$put('/api/v1/update_email', {
      email,
      password
    })
    userData.then((res) => {
      commit('isSucess', true);
      commit('setInProgress', false);
    })
    userData.catch((err) => {
      commit('setError', true)
    })
  },
  clearErrorMessage({ commit }) {
    commit('setError', false)
  }
};
