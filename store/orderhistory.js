
export const state = () => ({
  isInProgress: false,
  orderHistory: []
});

export const getters = {
  isInProgress: state => state.isInProgress,
  orderHistory: state => state.orderHistory
};

export const mutations = {
  setOrderHistory(state, history) {
    state.orderHistory = history;
  },
  setInProgress(state, status) {
    state.isInProgress = status;
  }
};

export const actions = {
  fetchOrderHistory({ commit }) {
    var resp = this.$axios.$get('/api/v1/delivery_history')
    commit('setInProgress', true)
    resp.then((res) => {
      commit('setOrderHistory',res['deliveries'])
      commit('setInProgress', false)
    }).catch((res) => {
      commit('setInProgress', false)
    })
  }
};
