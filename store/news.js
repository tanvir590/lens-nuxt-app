
export const state = () => ( {
  newsItems: [{}, {}, {}],
  isInProgress: false,
  isError: false
} );

export const getters = {
  getNewsItems: state => state.newsItems,
  isInProgress: state => state.isInProgress,
  isError: state => state.isError,

};

export const mutations = {
  setNewsItems( state, newsItems ) {
    state.newsItems = newsItems;
  },
  setInProgress(state, isInProgress){
    state.isInProgress = isInProgress;
  },
  setError(state, isError){
    state.isError = isError;
  },
};

export const actions = {
  fetchNews( { commit } ) {
    const news = this.$axios.$get( '/api/v1/news' );

    commit( 'setInProgress', true );

    news.then( res => {
      commit( 'setNewsItems', res.news );
      commit( 'setInProgress', false );
    } ).catch( () => {
      commit( 'setInProgress', false );
      commit( 'setNewsItems', [] );
    } );
  },
};
