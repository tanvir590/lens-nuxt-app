export const state = () => ({
  isSubmit: false,
  isError: false,
  InProgress: false
})

export const getters = {
  isSubmit: (state) => state.isSubmit,
  isError: (state) => state.isError,
  InProgress: (state) => state.InProgress
}

export const mutations = {
  isSubmit(state, status) {
    state.isSubmit = status
  },
  isError(state, status) {
    state.isError = status
  },
  InProgress(state, status) {
    state.InProgress = status
  }
}

export const actions = {
  // Post feedback api call
  postFeedback({ commit }, payload) {
    commit('isSubmit', false)
    commit('InProgress', true)
    commit('isError', false)
    this.$axios
      .$post('/api/v1/customer_opinion', payload)
      .then((res) => {
        commit('isSubmit', true)
        commit('InProgress', false)
      })
      .catch((err) => {
        commit('isError', true)
        commit('InProgress', false)
      })
  }

}