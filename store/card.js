

export const state = () => ({
  isSubmit: false,
  isError: false,
  isInProgress: false
})

export const getters = {
  isSubmit: state => state.isSubmit,
  isError: state => state.isError,
  isInProgress: state => state.isInProgress
}

export const mutations = {
  isSubmit(state, status) {
    state.isSubmit = status
  },
  isError(state, status) {
    state.isError = status
  },
  isInProgress(state, status) {
    state.isInProgress = status
  }
}

export const actions = {
  // Update card information
  updateCardInfo({ commit }, payload) {
    commit('isSubmit', false)
    commit('isError', false)
    commit('isInProgress', true)
    let data = {
      token: payload.token
    }
    this.$axios.$put('/api/v1/credit', data)
      .then(res => {
        commit('isSubmit', true)
        commit('isInProgress', false)
      })
      .catch(err => {
        commit('isError', true)
        commit('isInProgress', false)
      })
  },
  clearErrorMessage({ commit }) {
    commit('isError', false)
  }

}
