
export const state = () => ( {
  isInProgress: false,
  isInProgressDates: false,
  delivery: {},
  dates: {},
} );

export const getters = {
  isInProgress: state => state.isInProgress,
  isInProgressDates: state => state.isInProgressDates,
  getDelivery: state => state.delivery,
  getDates: state => state.dates,
};

export const mutations = {
  setInProgress( state, status ) {
    state.isInProgress = status;
  },
  setInProgressDates( state, status ) {
    state.isInProgressDates = status;
  },
  setDelivery( state, delivery ) {
    state.delivery = {
      ...state.delivery,
      ...delivery,
    };
  },
  setDates( state, dates ) {
    state.dates = dates;
  },
};

export const actions = {
  fetchDelivery( { commit } ) {
    const deliveryData = this.$axios.$get( '/api/v1/delivery' );

    commit( 'setInProgress', true );
    deliveryData.then( res => {
      commit( 'setDelivery', res );
      commit( 'setInProgress', false );
    } ).catch( () => {
      commit('setInProgress', false)
    } );
  },
  fetchDeliveryDates( { commit } ) {
    const deliveryData = this.$axios.$get( '/api/v1/delivery_dates' );

    commit( 'setInProgressDates', true );
    deliveryData.then( res => {
      commit( 'setDates', res );
      commit( 'setInProgressDates', false );
    } ).catch( () => {
      commit('setInProgressDates', false)
    } );
  },
  submitDeliveryDate( { commit }, date ) {
    const deliveryData = this.$axios.$put( '/api/v1/delivery', {
      delivery_date: date,
    } );
    commit( 'setInProgress', true );
    deliveryData.then( res => {
      commit( 'setDelivery', res );
      commit( 'setInProgress', false );
    } ).catch( () => {
      commit('setInProgress', false)
    } );
  }
};
