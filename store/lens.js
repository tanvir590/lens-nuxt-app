
export const state = () => ( {
  lenses: {},
  isInProgress: false,
  isError: false,
  isSuccess: false
} );

export const getters = {
  getLenses: state => state.lenses,
  isInProgress: state => state.isInProgress,
  isError: state => state.isError,
  isSuccess: state => state.isSuccess,

};

export const mutations = {
  lenseList( state, lenses ) {
    state.lenses = lenses;
  },
  setInProgress(state, isInProgress){
    state.isInProgress = isInProgress;
  },
  setError(state, isError){
    state.isError = isError;
  },
  setSuccess(state, isSuccess){
    state.isSuccess = isSuccess;
  },
};

export const actions = {
  fetchLenseData( { commit } ) {
    const LenseData = this.$axios.$get( '/api/v1/lens' );

    LenseData.then( res => {
      commit( 'lenseList', res );
    } );
  },

  updateUserLens({commit}, payload) {
    commit( 'setInProgress', true );
    commit( 'setError', false );
    commit( 'setSuccess', false );
    const {
      left_lens_id,
      right_lens_id
    } = payload
    const lensData = this.$axios.$put('/api/v1/lens', {
      left_lens_id,
      right_lens_id
    })
    lensData.then((res) => {
      if ( res.left_lens_id === left_lens_id && res.right_lens_id === right_lens_id ) {
        commit('user/setUser', res, { root: true } )
        commit( 'setSuccess', true );
      } else {
        commit( 'setError', true )
      }
      commit( 'setInProgress', false );
    })
    lensData.catch((err) => {
      commit( 'setError', true );
      commit( 'setInProgress', false );
    })
  }
};
