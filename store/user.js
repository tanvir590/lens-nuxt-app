export const state = () => ({
  user: {},
  isInProgress: false,
  isSubmitSend: false,
  isError: false,
  isSuccess: false,
  inviteCode: '',
  errorMessage: '',
  responseStatusCode: ''
})

export const getters = {
  getUser: (state) => state.user,
  isInProgress: (state) => state.isInProgress,
  getSubmitSend: (state) => state.isSubmitSend,
  isError: state => state.isError,
  isSuccess: state => state.isSuccess,
  inviteCode: (state) => state.inviteCode,
  errorMessage: (state) => state.errorMessage,
  responseStatusCode: (state) => state.responseStatusCode
}

export const mutations = {
  setUserInProgress(state, status) {
    state.isInProgress = status
  },
  setUser(state, user) {
    state.user = user
  },
  setSubmitSend(state, status) {
    state.isSubmitSend = status;
  },
  setError(state, isError) {
    state.isError = isError;
  },
  setSuccess(state, isSuccess) {
    state.isSuccess = isSuccess;
  },
  setInviteCode(state, inviteCode) {
    state.inviteCode = inviteCode;
  },
  errorMessage(state, status) {
    state.errorMessage = status
  },
  setResponseStatusCode(state, status){
    state.responseStatusCode = status
  }
}

export const actions = {
  fetchUser({ commit }) {
    commit('setResponseStatusCode', 0)
    const userData = this.$axios.$get('/api/v1/user')

    commit('setUserInProgress', true)
    userData.then((res) => {
      commit('setUser', res)
      commit('setUserInProgress', false)
      commit('setResponseStatusCode', 200)
    }).catch((err) => {
      commit('setUserInProgress', false)
      commit('setResponseStatusCode', err.response.status)
    });
  },
  // Update user address api call
  updateUserAddress({ commit }, payload) {
    const user = this.state.user.user
    let newUser = {}
    const {
      name,
      zipcode,
      prefecture,
      address_1,
      address_2,
      phone_number,
      email,
    } = payload
    if (name !== user.name) {
      newUser.name = name
    }
    if (zipcode !== user.zipcode) {
      newUser.zipcode = zipcode
    }
    if (prefecture !== user.prefecture) {
      newUser.prefecture = prefecture
    }
    if (address_1 !== user.address_1) {
      newUser.address_1 = address_1
    }
    if (address_2 !== user.address_2) {
      newUser.address_2 = address_2
    }
    if (phone_number !== user.phone_number) {
      newUser.phone_number = phone_number
    }
    newUser.check_email = email;

    const checkData = {
      name,
      zipcode,
      prefecture,
      phone_number,
      address_1,
      address_2,
      check_email: user.email,
    };

    const formData = new FormData();

    Object.keys( checkData ).forEach( dataKey => {
      formData.append( dataKey, checkData[dataKey] )
    } );

    const check_user = this.$axios.$post('/api/v1/validate_user_info',formData);
    commit('setSubmitSend', true)
    commit('setError', false)
    commit('setSuccess', false);

    check_user.then(res => {
      if(res.valid == true){
        const userData = this.$axios.$put('/api/v1/user', newUser)
        userData.then((res) => {
          commit('setSubmitSend', false)
          commit('setSuccess', true);
          commit('setUser', res)
        }).catch((err) => {
          commit('setSubmitSend', false)
          commit('setError', true)
          commit('errorMessage', err.response.data.message)
        })
      } else {
        commit('setSubmitSend', false)
        commit('setError', true)
        commit('errorMessage', res.error_field.map( i => i.message ).join( '\n ' ))
      }
    }).catch( ( err ) => {
      commit('setSubmitSend', false)
      commit('setError', true)
      commit('errorMessage', err.response.data.message)
    } )
  },


  leaveUser({ commit }, payload) {
    commit('setSuccess', false);
    commit('setInProgress', true);
    commit('setError', false);
    const {
      reason
    } = payload
    const userData = this.$axios({
      method: 'DELETE',
      url: '/api/v1/user',
      data: { body: reason }
    })
    userData.then((res) => {
      commit('setSuccess', true)
    })
    userData.catch((err) => commit('setError', true))
  },

  faceInviteCode({ commit }) {
    const inviteCode = this.$axios.$get('/api/v1/invite_code')
    inviteCode.then((res) => {
      commit('setInviteCode', res)
    })
  },
  clearErrorMessage({ commit }) {
    commit('setError', false)
  }
}
