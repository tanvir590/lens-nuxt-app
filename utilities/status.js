export const orderStatusCode = {
  SUBSCRIBE: 1,
  STOPPED: 2,
  CREDIT_CARD_ERROR: 3,
};

export const deliveryStatus = {
  NEW_ORDER: '新規受付',
  DELIVERY_RESERVATION: '発送予約',
  AUTH_SALES_ERROR: '仮売上エラー',
  DELIVERY_COMPLETE: '発送完了',
  PAYMENT_COMPLETE: '入金完了',
  ACTUAL_SALES_ERROR: '実売上エラー',
  POST_COMPLETE: '投函完了',
  CANCEL: 'キャンセル',
}