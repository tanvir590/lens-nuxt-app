export default( { store } ) => {
  const token = store.$cookies.get('user_token')
  if(token){
    store.commit('setLogin', token)
    store.$axios.defaults.headers.common['platform'] = 'web';
    store.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
  }
}