export default function ({ store, redirect, route }) {
  const loggedStatus = store.getters['isLoggedIn'];
  const ignoreRoute = [
    '/agreement',
    '/agreement/',
    '/policy',
    '/policy/',
    '/company',
    '/company/',
    '/law',
    '/law/',
    '/login',
    '/login/',
    '/mypage/password_reset',
    '/mypage/password_reset/',
    '/order',
    '/order/',
    '/order/address',
    '/order/address/',
    '/order/credentials',
    '/order/credentials/',
    '/order/payment_info',
    '/order/payment_info/',
    '/order/confirm',
    '/order/confirm/',
    '/order/thanks',
    '/order/thanks/',
    '/mypage/leave/comp',
    '/mypage/leave/comp/',
    '/index',
    '/'
  ];

  if ( ( route.path === '/mypage/edit_password' || route.path === '/mypage/edit_password/' ) && route.query.reset_token !== undefined ) {
    return;
  }

  if (loggedStatus === true) {
    if (route.path === '/login' || route.path === '/' || route.path === '/order') {
      return redirect('/home');
    }
    return;
  } else {
    if (ignoreRoute.indexOf(route.path) !== -1) {
      return;
    }
    return redirect('/login?to='+route.path);
  }
}